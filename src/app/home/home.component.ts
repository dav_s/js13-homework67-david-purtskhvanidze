import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { Subscription } from 'rxjs';
import { MealService } from '../shared/meal.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  meals: Meal[] = [];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private mealService: MealService) { }

  ngOnInit(): void {
    this.meals = this.mealService.getMeals();
    this.mealsChangeSubscription = this.mealService.mealChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });
    this.mealsFetchingSubscription = this.mealService.mealFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.mealService.fetchMeals();
  }

  getCaloriesSum() {
    return this.meals.reduce((sum, item) => {
      return sum + item.getCalorie();
    }, 0);
  }

  removeMeal(id: string) {
    const next = () => {
      this.mealService.fetchMeals();
    };
    this.mealService.removeMeal(id).subscribe(next);
  }

  ngOnDestroy() {
    this.mealsChangeSubscription.unsubscribe();
    this.mealsFetchingSubscription.unsubscribe();
  }
}
