import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MealService } from '../shared/meal.service';
import { Subscription } from 'rxjs';
import { Meal } from '../shared/meal.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {
  @ViewChild('f') mealForm!: NgForm;

  isEdit = false;
  editedId = '';

  isUploading = false;
  mealUploadingSubscription!: Subscription;

  constructor(
    private mealService: MealService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}


  ngOnInit(): void {
    this.mealUploadingSubscription = this.mealService.mealUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });

    this.route.params.subscribe((params: Params) => {
      this.editedId = params['id'];
    });

    if (this.editedId != undefined) {
      this.mealService.fetchMeal(this.editedId).subscribe(result => {
        this.isEdit = true;
        const meal = <Meal>result;

        this.setFormValue({
          mealTime: meal.mealTime,
          description: meal.description,
          calorie: meal.calorie,
        });
      });
    } else {
      this.isEdit = false;
      this.editedId = '';
      this.setFormValue({
        mealTime: '',
        description: '',
        calorie: '',
      });
    }
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.mealForm.form.setValue(value);
    });
  }

  saveMeal() {
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(
      id,
      this.mealForm.value.mealTime,
      this.mealForm.value.description,
      this.mealForm.value.calorie,
    );

    const next = () => {
      this.mealService.fetchMeals();
      void this.router.navigate(['/'], {relativeTo: this.route});
    };

    if (this.isEdit) {
      this.mealService.editMeal(meal).subscribe();
    } else {
      this.mealService.addMeal(meal).subscribe(next);
    }
  }

  ngOnDestroy(): void {
    this.mealUploadingSubscription.unsubscribe();
  }
}
